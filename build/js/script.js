class Todolists {
    constructor(mainTodolistId, config) {
        //		taskClassName - название класса, которое будет иметь каждое дело, для стилизации (не обязательный)
        //		btnDelClassName - название класса, для кнопки удаления одного дела (не обязательный)
        //		btnAll - кнопка All, для возможности удаления всех дел
        //		btnActive - кнопка Active, для возможности удаления не выполненных дел
        //		btnCompleted - кнопка completed, для возможности удаления выполненных дел
        //		blockInfo - место для отбражения количества оставшихся дел
        //		btnDelete - кнопка для удаления нескольких дел сразу

        this._mainTodosId		= mainTodolistId;
        this.$input 			= config.input;
        this.$tasks 			= config.tasks;
        this._taskClassName 	= config.taskClassName;
        this._btnDelClassName	= config.btnDelClassName;
        this._id 				= ++Todolists._countLists;
        this._counterForId		= 0;
        this.$btnAll			= config.btnAll;
        this.$btnActive			= config.btnActive;
        this.$btnCompleted		= config.btnCompleted;
        this.$blockInfo			= config.blockInfo;
        this.$btnDelete 		= config.btnDelete;

        if (this._taskClassName == undefined) {
            this._taskClassName = _mainTodosId + "task";
        }

        this._btnAllOn			= false;
        this._btnActiveOn		= false;
        this._btnCompletedOn	= false;

        this._tasks = [];
        this._tasksInfoForLocal = [];

        this.$input.addEventListener("keyup", this.addTask.bind(this));
        this.$btnAll.addEventListener("click", this.btnAllClickHandler.bind(this));
        this.$btnActive.addEventListener("click", this.btnActiveClickHandler.bind(this));
        this.$btnCompleted.addEventListener("click", this.btnCompletedClickHandler.bind(this));
        this.$btnDelete.addEventListener("click", this.btnDeleteClickHandler.bind(this));
        this.$tasks.addEventListener('click', this.btnDeleteOneTaskClick.bind(this));

        // console.log(this.$input);

        // function(event) {
        //     if (event.target.classList[0] != 'del_task') return;
        //     event.target.parentNode.remove();
        // };

        if (localStorage.getItem("todosTasks" + this._id)) {

            this._tasksInfoForLocal 	= JSON.parse(localStorage.getItem("todosTasks" + this._id));
            this._counterForId 			= Number(localStorage.getItem("counterTasks" + this._id));
    
            for (let i in this._tasksInfoForLocal) {
    
                this.addTask({keyCode: 13},
                this._tasksInfoForLocal[i].text, 
                this._tasksInfoForLocal[i].checked,
                this._tasksInfoForLocal[i].spanId,
                this._tasksInfoForLocal[i].checkboxId);
    
            }
    
        }

        // this.$tasks.onclick = function(event) {
        //     let td = event.target.closest('span'); // (1)
    
        //     if (!td) return; // (2)
    
        //     if (!table.contains(td)) return; // (3)
    
        //     td.parentNode.remove(); // (4)
        // };

    }

    btnActiveClickHandler() {

        if (this._btnActiveOn == true) {
    
            this.$btnDelete.innerText = "";
            this.$blockInfo.innerText = "";
            this.$btnActive.style.color = "#BEB6B6";
    
            for (let task in this._tasks)
                if (this._tasks[task].taskCheckbox.checked == false)
                    this._tasks[task].taskBlock.style.boxShadow = 'none';
    
            this._btnActiveOn = false;
    
            return;
    
        }
    
        for (let task in this._tasks)
                this._tasks[task].taskBlock.style.boxShadow = 'none';
    
        for (let task in this._tasks)
            if (this._tasks[task].taskCheckbox.checked == false)
                this._tasks[task].taskBlock.style.boxShadow = '0 0 5px inset';
    
        this.$btnDelete.innerText = "Clear active tasks";
    
        this.$btnAll.style.color = "#BEB6B6";
        this.$btnActive.style.color = '#D3D3D3';
        this.$btnCompleted.style.color = '#BEB6B6';
    
        this._btnAllOn			= false;
        this._btnActiveOn		= true;
        this._btnCompletedOn	= false;
    
        let activeCount = 0;
    
        for (let task in this._tasks)
            if (this._tasks[task].taskCheckbox.checked == false)
                activeCount += 1;
    
        if (activeCount == 0)
            this.$blockInfo.innerText = "No tasks";
        else if (activeCount == 1)
            this.$blockInfo.innerText = "1 item left";
        else
            this.$blockInfo.innerText = activeCount + " items left";
    
    }

    addTask(event, textFromLocal, status, spanid, checkboxid) {
	
        //		span для значка крестика (удаление дела)
    
        let taskText = this.$input.value;
        if (textFromLocal != undefined) taskText = textFromLocal;
        if (event.keyCode != 13 || taskText == "") return;
    
        // if (textFromLocal == undefined) {
            this._counterForId += 1;
        // }
    
        let task 		= document.createElement("li"),
            taskDiv     = document.createElement("div"),
            checkbox 	= document.createElement("input"),
            label 		= document.createElement("label"),
            span 		= document.createElement("span");
    
        checkbox.type 	= "checkbox";
        checkbox.classList.add('checkbox');
        checkbox.id 	= "checkbox" + this._id + this._counterForId;
        label.innerText = taskText;
        label.htmlFor	= "checkbox" + this._id + this._counterForId;
        label.classList.add('label');
        span.innerHTML 	= "&times;";
        span.id 		= this._mainTodosId + "span" + this._id + this._counterForId;
        
        span.classList.add('del_task');
    
        if (textFromLocal != undefined) {
    
            checkbox.id 	= checkboxid;
            span.id 		= spanid;
            label.htmlFor	= checkboxid;
            
        }
    
        taskDiv.append(checkbox);
        taskDiv.append(label);
    
        taskDiv.classList.add('text-break');
    
        taskDiv.classList.add('d-flex');
        taskDiv.classList.add('align-items-center');
        
        task.append(taskDiv);
        task.append(span);
    
        // task.append(checkbox);
        // task.append(label);
        // task.append(span);
    
        // if (this._taskClassName != undefined) {
        // 	task.className = this._taskClassName;
        // }
    
        if (this._btnDelClassName != undefined) {
            span.classList.add(this._btnDelClassName);
        }
    
        let taskInformation = {
    
            taskBlock: 		task,
            taskCheckbox: 	checkbox,
            taskSpanId: 	span.id,
    
        }
    
        let self = this;
    
        let taskInformationForlocal = {
    
            id: 			self._counterForId,
            text: 			taskText,
            checked: 		false,
            spanId: 		span.id,
            checkboxId:		checkbox.id,
    
        }
    
        this._tasks.push(taskInformation);
    
        if (textFromLocal == undefined) {
    
            this._tasksInfoForLocal.push(taskInformationForlocal);
    
        }
    
        // span.addEventListener("click", deleteTask.bind(this));
    
        label.addEventListener("click", setInfoAboutTasksCount.bind(this));
        label.addEventListener("click", changeCheckboxStatus.bind(this));
    
        function changeCheckboxStatus(event) {
    
            // if (taskInformationForlocal.checked == false) {
            // 	taskInformationForlocal.checked = true;
            // } else if (taskInformationForlocal.checked = true) {
            // 	taskInformationForlocal.checked = false;
            // }
    
            if (event.target.classList[0] != 'label') return;
    
            console.log(event.target.parentNode);
            // .parentNode.querySelector('span')
    
            for (let i in this._tasksInfoForLocal) {
    
                if (this._tasksInfoForLocal[i].spanId == event.target.parentNode.parentNode.querySelector('.del_task').id) {
    
                    if (this._tasksInfoForLocal[i].checked == false) {
                        this._tasksInfoForLocal[i].checked = true;
                    } else if (this._tasksInfoForLocal[i].checked = true) {
                        this._tasksInfoForLocal[i].checked = false;
                    }
    
                }
    
            }
    
            this.setLocalStorage.call(this);
    
        }
    
        // function deleteTask() {
            
        // 	for (let task in this._tasksInfoForLocal)
        // 		if (this._tasksInfoForLocal[task].spanId == span.id) {
        // 			this._tasksInfoForLocal.splice(task, 1);
        // 			break;
        // 		}
    
        // 	for (let task in this._tasks)
        // 		if (this._tasks[task].taskSpanId == span.id) {
        // 			this._tasks.splice(task, 1);
        // 			break;
        //         }
                
        // 	span.parentNode.remove();
        // 	this._counterTasks -= 1;
    
        // 	setInfoAboutTasksCount.call(this);
        // 	this.setLocalStorage.call(this);
    
        // }
    
        function setInfoAboutTasksCount() {
    
            switch(true) {
    
                case this._btnAllOn:
                    this._btnAllOn = false;
                    setTimeout(this.btnAllClickHandler.bind(this), 10);
                    break;
    
                case this._btnActiveOn: 
                    this._btnActiveOn = false;
                    setTimeout(this.btnActiveClickHandler.bind(this), 10);
                    break;
    
                case this._btnCompletedOn:
                    this._btnCompletedOn = false; 
                    setTimeout(this.btnCompletedClickHandler.bind(this), 10);
                    break;
    
            }
    
        }
        
        task.classList.add("list-group-item");
        task.classList.add("d-flex");
        task.classList.add("justify-content-between");
        task.classList.add("align-items-center");
        task.classList.add("rounded-0");
        task.classList.add("task");
    
        this.$tasks.prepend(task);
        this._counterTasks += 1;
    
        if (status == true) {
            checkbox.click();
        }
    
        this.$input.value = "";
    
        setInfoAboutTasksCount.call(this);
        this.setLocalStorage.call(this);
    
        console.log(this._tasks);
    
    };

    btnAllClickHandler() {

        if (this._btnAllOn == true) {
    
            this.$btnDelete.innerText = "";
            this.$blockInfo.innerText = "";
            this.$btnAll.style.color = "#BEB6B6";
    
            for (let task in this._tasks)
                this._tasks[task].taskBlock.style.boxShadow = 'none';
    
            this._btnAllOn = false;
    
            return;
    
        }
    
        for (let task in this._tasks)
            this._tasks[task].taskBlock.style.boxShadow = '0 0 5px inset';
    
        this.$btnDelete.innerText = "Clear all tasks";
    
        this.$btnAll.style.color = "#D3D3D3";
        this.$btnActive.style.color = '#BEB6B6';
        this.$btnCompleted.style.color = '#BEB6B6';
    
        this._btnAllOn			= true;
        this._btnActiveOn		= false;
        this._btnCompletedOn	= false;
    
        if (this._tasks.length == 0)
            this.$blockInfo.innerText = "No tasks";
        else if (this._counterTasks - 1 == 1)
            this.$blockInfo.innerText = "1 item left";
        else
            this.$blockInfo.innerText = this._tasks.length + " items left";
    
    }

    btnCompletedClickHandler() {

        if (this._btnCompletedOn == true) {
    
            this.$btnDelete.innerText = "";
            this.$blockInfo.innerText = "";
            this.$btnCompleted.style.color = "#BEB6B6";
    
            for (let task in this._tasks)
                if (this._tasks[task].taskCheckbox.checked == true)
                    this._tasks[task].taskBlock.style.boxShadow = 'none';
    
            this._btnCompletedOn = false;
    
            return;
    
        }
    
        for (let task in this._tasks)
                this._tasks[task].taskBlock.style.boxShadow = 'none';
    
        for (let task in this._tasks)
            if (this._tasks[task].taskCheckbox.checked == true)
                this._tasks[task].taskBlock.style.boxShadow = '0 0 5px inset';
    
        this.$btnDelete.innerText = "Clear completed tasks";
    
        this.$btnAll.style.color = "#BEB6B6";
        this.$btnActive.style.color = '#BEB6B6';
        this.$btnCompleted.style.color = '#D3D3D3';
    
        this._btnAllOn			= false;
        this._btnActiveOn		= false;
        this._btnCompletedOn	= true;
    
        let completedCount = 0;
    
        for (let task in this._tasks)
            if (this._tasks[task].taskCheckbox.checked == true)
                completedCount += 1;
    
        if (completedCount == 0)
            this.$blockInfo.innerText = "No tasks";
        else if (completedCount == 1)
            this.$blockInfo.innerText = "1 item left";
        else
            this.$blockInfo.innerText = completedCount + " items left";
    
    }

    btnDeleteOneTaskClick() {

        if (event.target.classList[0] != 'del_task') return;
    
        for (let task in this._tasksInfoForLocal) {
            if (this._tasksInfoForLocal[task].spanId == event.target.id) {
                this._tasksInfoForLocal.splice(task, 1);
                break;
            }
        }
    
        for (let task in this._tasks) {
            if (this._tasks[task].taskSpanId == event.target.id) {
                this._tasks.splice(task, 1);
                break;
            }
        }
    
        function setInfoAboutTasksCount() {
    
            switch(true) {
    
                case this._btnAllOn:
                    this._btnAllOn = false;
                    setTimeout(this.btnAllClickHandler.bind(this), 10);
                    break;
    
                case this._btnActiveOn: 
                    this._btnActiveOn = false;
                    setTimeout(this.btnActiveClickHandler.bind(this), 10);
                    break;
    
                case this._btnCompletedOn:
                    this._btnCompletedOn = false; 
                    setTimeout(this.btnCompletedClickHandler.bind(this), 10);
                    break;
    
            }
    
        }
    
        event.target.parentNode.remove();
    
        this._counterTasks -= 1;
    
        setInfoAboutTasksCount.call(this);
        this.setLocalStorage.call(this);
    }

    btnDeleteClickHandler() {

        if (!confirm("Delete tasks?")) return;
    
        switch(true) {
    
            case this._btnAllOn: {
    
                for (let task in this._tasks)
                    this._tasks[task].taskBlock.remove();
    
                this._tasks 			= [];
                this._tasksInfoForLocal	= [];
                this._counterForId 		= 0;
                this._counterTasks 		= 1 ;
    
                this._btnAllOn = false;
                setTimeout(this.btnAllClickHandler.bind(this), 10);
    
                break;
    
            }
    
            case this._btnActiveOn: {
    
                for (var task in this._tasks)
                    if (this._tasks[task].taskCheckbox.checked == false) {
    
                        this._tasks[task].taskBlock.remove();
                        this._tasks[task] = null;
                        
                    }
    
                for (let i in this._tasksInfoForLocal) {
                    if (this._tasksInfoForLocal[i].checked == false) {
                        this._tasksInfoForLocal[i] = null;
                    }
                }
    
                this._tasks 			= this._tasks.filter((item) => item != null);
                this._tasksInfoForLocal = this._tasksInfoForLocal.filter((item) => item != null);
                this._counterTasks 		= this._tasks.length + 1;
    
                this._btnActiveOn = false;
                setTimeout(this.btnActiveClickHandler.bind(this), 10);
    
                break;
            }
    
            case this._btnCompletedOn: {
                
                for (var task in this._tasks)
                    if (this._tasks[task].taskCheckbox.checked == true) {
    
                        this._tasks[task].taskBlock.remove();
                        this._tasks[task] = null;
                        
                    }
    
                for (let i in this._tasksInfoForLocal) {
                    if (this._tasksInfoForLocal[i].checked == true) {
                        this._tasksInfoForLocal[i] = null;
                    }
                }
    
                this._tasks 			= this._tasks.filter((item) => item != null);
                this._tasksInfoForLocal = this._tasksInfoForLocal.filter((item) => item != null);
                this._counterTasks 		= this._tasks.length + 1;
    
                this._btnCompletedOn = false;
                this.btnCompletedClickHandler.call(this);
    
                break;
            }
    
        }
    
        this.setLocalStorage.call(this);
    
    }

    setLocalStorage() {

        localStorage.setItem("todosTasks" + this._id, JSON.stringify(this._tasksInfoForLocal));
        localStorage.setItem("counterTasks" + this._id, this._counterForId);
    
    }
}

Todolists._countLists = 0;